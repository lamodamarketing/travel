#-*- coding: utf-8 -*-
from wtforms import Form, BooleanField, TextField, RadioField, TextAreaField, SelectField, ValidationError, IntegerField, validators

class MainForm(Form):
    name = TextField(u'Имя')
    mail = TextField(u'E-mail')
    phone = TextField(u'Телефон')
    travel = RadioField(u'Способ передвижения', [validators.AnyOf(['bus', 'car'], message=u'Выберите вид транспорта')], choices=[('bus', u'Автобус'), ('car', u'Машина')])
    comment = TextAreaField(u'Комментарий')
    is_driver = BooleanField()
    seats = IntegerField()

    def validate_name(self, field):
        if field.data:
            if len(field.data) < 3:
                raise ValidationError(u'Имя должно быть не короче трёх букв')
            elif len(field.data) > 50:
                raise ValidationError(u'Имя должно быть не длиннее пятидесяти букв')
        else:
            raise ValidationError(u'Введите имя')


class EnrollForm(Form):
    name = TextField('', [validators.Length(min=3, max=50)])

class ChangeContactsForm(Form):
    hidden_name = TextField()
    contact = TextField()

class ChangeTravelForm(Form):
    hidden_name = TextField()
    travel = SelectField(choices=[('bus', u'автобус'), ('car', u'машина')])
