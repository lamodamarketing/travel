from conf import db

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)
    travel = db.Column(db.String(3))
    seats = db.Column(db.Integer)
    is_driver = db.Column(db.Boolean)
    car_owner = db.Column(db.String(50))
    phone = db.Column(db.String)
    email = db.Column(db.String)
    comment = db.Column(db.Text)

    def __init__(self, name, travel, seats, is_driver, car_owner, phone, email, comment):
        self.name = name
        self.travel = travel
        self.seats = seats
        self.is_driver = is_driver
        self.car_owner = car_owner
        self.phone = phone
        self.email = email
        self.comment = comment

    def as_dict(self):
        return {'id': self.id,
                'name': self.name,
                'travel': self.travel,
                'seats': self.seats,
                'is_driver': self.is_driver,
                'car_owner': self.car_owner,
                'phone': self.phone,
                'email': self.email,
                'comment': self.comment}


class DatabaseManager():
    def __init__(self, database):
        self.dtbs = database

    def get_all_users(self):
        return [user.as_dict() for user in self.dtbs.query.all()]

    def create_user(self, array):
        name, travel, seats, is_driver, car_owner, phone, email, comment = array
        new_user = self.dtbs(name, travel, seats, is_driver, car_owner, phone, email, comment)
        db.session.add(new_user)
        return new_user

    def get_user_by_name(self, name):
        return self.dtbs.query.filter_by(name=name).first()

    def delete_user_for_name(self, name):
        user = self.get_user_by_name(name)
        if user.is_driver:
            psng = self.collect_driver_info(name)
            map(self.exclude_driver_from_info, psng)
            map(lambda x: self.update_data(x, 'travel', 'bus'), psng)
        db.session.delete(user)

    def update_data(self, name, column, data):
        user = self.get_user_by_name(name)
        setattr(user, column, data)

    def add_driver_to_info(self, name, car_owner):
        self.update_data(name, 'car_owner', car_owner)
        owner = self.get_user_by_name(car_owner)
        new_seats = owner.seats - 1
        self.update_data(car_owner, 'seats', new_seats)
        self.update_data(name, 'travel', 'car')

    def update_seats_info(self, name):
        user = self.get_user_by_name(name)
        if user.travel == 'bus':
            self.update_data(name, 'seats', None)

    def exclude_driver_from_info(self, name):
        user = self.get_user_by_name(name)
        if user.is_driver:
            self.update_data(name, 'seats', None)
            return
        self.update_data(name, 'car_owner', None)
        if user.car_owner:
            owner = self.get_user_by_name(user.car_owner)
            if owner and owner.seats:
                new_seats = owner.seats + 1
                self.update_data(owner.name, 'seats', new_seats)

    def collect_driver_info(self, driver):
        users = self.dtbs.query.filter_by(car_owner=driver).all()
        return [user.as_dict()['name'] for user in users]

    def total_amount(self):
        total = self.dtbs.query.filter_by(travel='bus').all()
        bus = len(total)
        car = len(self.get_all_users()) - bus
        return {'bus': bus, 'car': car}