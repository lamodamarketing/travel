#-*- coding: utf-8 -*-
from flask import render_template, request, redirect
from model.forms import MainForm, ChangeContactsForm, ChangeTravelForm, EnrollForm
from model.db_classes import User, DatabaseManager
from conf import app, db

def init_db():
    # db.drop_all()
    db.create_all()

@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()


@app.route('/info', methods=['POST', 'GET'])
def info():
    columns = ['name', 'travel', 'seats', 'is_driver', 'car_owner', 'phone', 'mail', 'comment']
    mng = DatabaseManager(User)
    main_form = MainForm(request.form)
    contacts_form = ChangeContactsForm(request.form)
    travel_form = ChangeTravelForm(request.form)
    if request.method == 'POST':

        if 'comment' in request.values:
            if main_form.validate():
                form_columns = ['name', 'mail', 'phone', 'travel', 'comment', 'is_driver', 'seats']
                arguments = dict(zip(form_columns, map(lambda x: main_form[x].data, form_columns)))
                if arguments['is_driver']:
                    arguments['car_owner'] = arguments['name']
                else:
                    arguments['car_owner'] = None
                if not mng.get_user_by_name(arguments['name']):
                    mng.create_user(arguments[i] for i in columns)
                db.session.commit()
                return redirect('/info')

        elif travel_form.validate():
            name, new_travel = travel_form.hidden_name.data, travel_form.travel.data
            if name and new_travel:
                mng.update_data(name, 'travel', new_travel)
                mng.update_seats_info(name)
                user = mng.get_user_by_name(name)
                if user.is_driver and user.travel != 'car':
                    passengers = mng.collect_driver_info(user.name)
                    map(mng.exclude_driver_from_info, passengers)
                    map(lambda x: mng.update_data(x, 'travel', 'bus'), passengers)
                elif user.travel != 'car':
                    mng.exclude_driver_from_info(name)
            db.session.commit()

        elif contacts_form.validate():
            if contacts_form.hidden_name.data:
                name, column = contacts_form.hidden_name.data[:-1], contacts_form.hidden_name.data[-1]
                if column == 'm':
                    column = 'email'
                else:
                    column = 'phone'
                mng.update_data(name, column, contacts_form.contact.data)
            db.session.commit()

    data = mng.get_all_users()
    total = mng.total_amount()
    return render_template('form.html', data=data, total=total, main_form=main_form, contacts_form=contacts_form, travel_form=travel_form)

@app.route('/info/<name>', methods=['GET', 'POST'])
def name_info(name):
    enroll_form = EnrollForm(request.form)
    mng = DatabaseManager(User)

    if request.method == 'POST':
        if 'delete' in request.values:
            mng.delete_user_for_name(name)
        if enroll_form.validate():
            psng_name = enroll_form.name.data
            if not mng.get_user_by_name(psng_name):
                mng.create_user([psng_name, 'car', None, 0, None, None, None, None])
            psng = mng.get_user_by_name(psng_name)
            if name != psng.car_owner and not psng.is_driver:
                if psng.car_owner:
                    new_seats = mng.get_user_by_name(psng.car_owner).seats + 1
                    mng.update_data(psng.car_owner,  'seats', new_seats)
                mng.add_driver_to_info(psng.name, name)
        db.session.commit()

    user = mng.get_user_by_name(name)
    passengers = None
    driver = None
    if user and user.car_owner:
        passengers = mng.collect_driver_info(user.car_owner)
        driver = mng.get_user_by_name(user.car_owner)
        if driver:
            driver = driver.name
        if user:
            user=user.as_dict()
    return render_template('name_info.html', data=user, psng=passengers, driver=driver, enroll_form=enroll_form)

if __name__ == '__main__':
    init_db()
    app.run(None, None, True)
